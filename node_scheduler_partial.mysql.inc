<?php

function node_scheduler_partial_by_type($type) {  // we need some cacheing here
	$sql = "select partial.pid, field, title from {partial}, {node_scheduler_partial_type_constraints} nspttc where partial.pid=nspttc.pid and nspttc.type='%s'";
	$result = db_query($sql, $type);
	while ($entry = db_fetch_object($result)) {
		$key = 'field_' . $entry->pid;
		if ($entry->field) {
			$key .= '-' . $entry->field;
		}
		$value = $entry->title;
		if ($entry->field) {
			$value .= '->' . $entry->field;
		}
		$retVal[$key] = $value;
	}
	return $retVal;
}

function node_scheduler_partial_get_type_constraints($pid) {
	$sql = "select * from {node_scheduler_partial_type_constraints} where pid=%d";
	$result = db_query($sql, $pid);
	while ($partial = db_fetch_object($result)) {
		$retVal[] = $partial;
	}
	return $retVal;
}

function node_scheduler_partial_get_field_type_constraints($pid, $field) {
	if ($field == 'node') {
		$field = null;
	}
	$sql = "select * from {node_scheduler_partial_type_constraints} where pid=%d and field='%s'";
	$result = db_query($sql, $pid, $field);
	while ($partial = db_fetch_object($result)) {
		$retVal[] = $partial;
	}
	return $retVal;
}

function node_scheduler_partial_insert_by_type($pid, $field, $type) {
	if (!empty($type)) {
		$sql = "insert into {node_scheduler_partial_type_constraints} (pid, field, type) values (%d, '%s', '%s')";
		if ($field == 'node') { // default is node
			$field = null;
		}
		db_query($sql, $pid, $field, $type);
	}
}

function node_scheduler_partial_delete($pid) {
	$sql = "delete from {node_scheduler_partial_type_constraints} where pid=%d";
	db_query($sql, $pid);
}

function node_scheduler_partial_get_partial_schedule($pid, $field) {
	if ($pid > 0) {
		if ($field == 'node') {
			$field = null;
		}
		$header = array(
											array('data'=>t('Type'), 'field'=>'type'),
											array('data'=>t('Title'), 'field'=>'title'),
											array('data'=>t('Field'), 'field'=>'field'),
											array('data'=>t('Link Type'), 'field'=>'params'),
											array('data'=>t('Start Date'), 'field'=>'start_date'),
											array('data'=>t('End Date'), 'field'=>'end_date'),
											t('Actions'),null
										);
		$tablesort = tablesort_sql($header);
		$sql = "select * from {node}, {node_schedule} where node.nid=node_schedule.nid and node_schedule.target_id=%d and field='%s'";
		$sql .= $tablesort;
		$result = db_query($sql, $pid, $field);
		return node_scheduler_db_result($result);
	}
}

function node_scheduler_partial_get_partial_schedule_current($pid, $field, $time) {
	$node = null;
	if ($field == 'node') {
		$field = null;
	}
	if (($pid > 0) && $time) {
		$result = db_query('select * from {node_schedule} where target_id=%d and field="%s" and start_date <= %d and (end_date = "" or end_date > %d) order by start_date desc', $pid, $field, $time, $time);
		if ($entry = db_fetch_object($result)) {
			$node = node_load($entry->nid);
			$node->schedule[0] = new stdClass();
			$node->schedule[0]->target_id = $entry->target_id;
			$node->schedule[0]->field = $entry->field;
			$node->schedule[0]->params = $entry->params;
		}
	}
	return $node;
}
